package main

import (
	"fmt"
)

func main() {
	var a int
	fmt.Println("masukkan angka bin : ")
	fmt.Scanf("%d", &a)
	binaryToDecimal(a)

	var b int
	fmt.Println("masukkan angka desimal : ")
	fmt.Scan(b)
	decimalToBinary(b)

}

func binaryToDecimal(n int) {
	angka := n
	nilai := 0

	a := 1

	temp := angka

	for temp > 0 {
		akhir := temp % 10
		temp = temp / 10
		nilai += akhir * a
		a = a * 2

	}

	fmt.Println(nilai)
}

func decimalToBinary(n int) {
	var bin [32]int
	i := 0

	for n > 0 {
		bin[i] = n % 2
		n = n / 2
		i++
	}

	for j := i - 1; j >= 0; j-- {
		fmt.Print(bin[j])
	}
}
